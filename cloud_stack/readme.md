# Cloud-stack
Contient quatre conteneurs et un réseau : une base de données MySQL, une instance Nextcloud, une instance de Nginx Proxy Manager pour pouvoir générer des certificats SSL pour protéger le traffic et une instance de collabora pour l'édition de fichiers LibreOiffce collaborativement.
Une fois les conteneurs lancés, il faut créer une base de données et un utilisateur pour pouvoir configurer Nextcloud avec cette base de données (très recommandé).
